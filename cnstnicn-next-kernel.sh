#!/bin/sh

set -ex
shopt -s nullglob

TODO=/var/lib/cnstnicn

# Not really a loop. Only one iteration will execute.
for k in $TODO/kernel-core-*.rpm; do
	[[ "$k" =~ /kernel-core-(.*)\.rpm$ ]] || exit 1
	nvra="${BASH_REMATCH[1]}"

	echo $nvra
	dnf install -y $TODO/kernel-*$nvra.rpm

	kexec.sh $nvra

	# Shutdown for kexec is async
	exit 0
done

# If we get here, there are no more kernels to test.
systemctl disable cnstnicn-next-kernel.service cnstnicn-submit.service
echo "Finished gathering data for Consistent NIC naming."
