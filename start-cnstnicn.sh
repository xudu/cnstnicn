#!/bin/sh

set -ex

systemctl enable cnstnicn-submit.service cnstnicn-next-kernel.service

cd /var/lib/cnstnicn
download-kernels.sh < /usr/share/cnstnicn/kernels.txt

# Might as well submit data for the currently running kernel
cnstnicn-submit.sh

# Install a kernel & boot it
cnstnicn-next-kernel.sh
