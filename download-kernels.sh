#!/bin/bash

# Reads a list of kernel-N-V-R from stdin and downloads the RPMs.

read KERNEL_DOWNLOAD_URL < /usr/share/cnstnicn/kernel_download_url

split_into_n_v_r() {
	NVR="$1"

	IFS=- read RR RV RN <<-EOF
		$(printf "%s" "$NVR" | rev)
	EOF

	N="$(printf "%s" "$RN" | rev)"
	V="$(printf "%s" "$RV" | rev)"
	R="$(printf "%s" "$RR" | rev)"

	printf "%s %s %s\n" "$N" "$V" "$R"
}

curl -fLZ $(while read nvr; do
	read n v r <<-EOF
		$(split_into_n_v_r "$nvr")
	EOF

	if [ "$n" != "kernel" ]; then
		echo "Name $n != kernel"
		exit 1
	fi

	# kernel-modules-core does not exist for older kernel builds.
	# We use curl with "-f" so that it reports an error and does not save
	# an error page.
	SUB_NAMES="kernel kernel-core kernel-modules kernel-modules-core"

	for sub in $SUB_NAMES; do
		echo " -O $KERNEL_DOWNLOAD_URL/$v/$r/x86_64/$sub-$v-$r.x86_64.rpm"
	done
done)
