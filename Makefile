.PHONY: help install dist srpm mockbuild clean

version := $(shell cat version)

bindir = /usr/bin
unitdir = /usr/lib/systemd/system
datadir = /usr/share
sharedstatedir = /var/lib

binfiles = cnstnicn-next-kernel.sh cnstnicn-submit.sh download-kernels.sh kexec.sh start-cnstnicn.sh
unitfiles = cnstnicn-next-kernel.service cnstnicn-submit.service
datafiles = kernels.txt kernel_download_url logger_args version
buildfiles = Makefile cnstnicn.spec.in
allsrc = $(binfiles) $(unitfiles) $(datafiles) $(buildfiles)

help:
	@echo "Possible targets:"
	@echo "    make install DESTDIR=<destdir>"
	@echo "    make dist"
	@echo "    make srpm"
	@echo "    make mockbuild"
	@echo "    make clean"

install:
	install -d $(DESTDIR)$(bindir)
	install -d $(DESTDIR)$(unitdir)
	install -d $(DESTDIR)$(datadir)/cnstnicn
	install -d $(DESTDIR)$(sharedstatedir)/cnstnicn
	install -p -m755 $(binfiles)  $(DESTDIR)$(bindir)
	install -p -m644 $(unitfiles) $(DESTDIR)$(unitdir)
	install -p -m644 $(datafiles) $(DESTDIR)$(datadir)/cnstnicn

dist: cnstnicn-$(version).tar.xz
srpm: cnstnicn-$(version)-1.src.rpm
mockbuild: result/cnstnicn-$(version)-1.noarch.rpm

cnstnicn-$(version).tar.xz: $(allsrc)
	tar --transform 's#^#cnstnicn-$(version)/#' -cJvf $@ $^

cnstnicn.spec: cnstnicn.spec.in version
	sed -e 's/@VERSION@/$(version)/;s/@DATE@/$(shell date "+%a %b %d %Y")/' $< > $@

cnstnicn-$(version)-1.src.rpm: cnstnicn.spec cnstnicn-$(version).tar.xz
	rpmbuild -bs -D '_topdir .' -D '_srcrpmdir .' -D '_rpmdir .' -D '_sourcedir .' -D '_specdir .' $<

result/cnstnicn-$(version)-1.noarch.rpm: cnstnicn-$(version)-1.src.rpm
	mock -r centos-stream-9-x86_64 --resultdir=result $<

clean:
	rm -f cnstnicn.spec cnstnicn-*.tar.xz cnstnicn-*.src.rpm
	rm -rf BUILD BUILDROOT result
