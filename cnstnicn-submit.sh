#!/bin/bash

MAGIC=CNSTNICN
read VERSION < /usr/share/cnstnicn/version
read LOGGER_ARGS < /usr/share/cnstnicn/logger_args

# List of attributes from device_get_sysattr_*_filtered calls from Lukas's MR:
#   https://github.com/systemd/systemd/pull/30397
# XXX: The commented out attributes do not appear on netdevs, but on parent (PCI) devices.
#      I don't think the driver influences those, so hopefully it's OK to ignore this.
ATTRS=(
#	'acpi_index'
	'addr_assign_type'
	'address'
#	'ari_enabled'
	'dev_id'
	'dev_port'
#	'function_id'   # s390
	'iflink'
#	'index'
#	'label'
#	'modalias'
	'phys_port_name'
	'type'
)

kernelver="$(uname -r)"

for netdev in /sys/class/net/*; do
	[ "$netdev" = /sys/class/net/lo ] && continue

	netdevname="$(basename "$netdev")"
	driver=$(basename "$(readlink "$netdev/device/driver")")

	printf "%s;%s;%s;%s;%s;" "$MAGIC" "$VERSION" "$kernelver" "$driver" "$netdevname"
	for attr in "${ATTRS[@]}"; do
		val=
		read val < "$netdev/$attr" 2>/dev/null
		printf "%s;%s;" "$attr" "$val"
	done
	printf "\n"
done | logger $LOGGER_ARGS

# Delete the RPMs of the current kernel from the to-do directory.
rm -f /var/lib/cnstnicn/kernel*-$kernelver.rpm
